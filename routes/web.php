<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('home', 'HomeController@index')->name('home');

//Notification Routes
Route::get('notification/center', 'NotificationController@showCenter');
Route::post('notification/send', 'NotificationController@send');
Route::post('notification/receive', 'NotificationController@receive');

//PC Terminal Routes
Route::get('terminal', 'TerminalController@index')->name('terminal');
Route::post('terminal/create', 'TerminalController@create');
Route::post('terminal/info', 'TerminalController@getTerminalInfo');

//Equipment Routes
Route::get('equipment/inventory', 'EquipmentController@showInventory')->name('equip.inventory');
Route::get('equipment/status', 'EquipmentController@showStatus')->name('equip.status');
Route::post('equipment/create', 'EquipmentController@create');
Route::post('equipment/update', 'EquipmentController@update');
Route::post('equipment/info', 'EquipmentController@showEquipmentData');
Route::post('equipment/inventory/chart','EquipmentController@showInventoryGraph');

//Student Details Routes
Route::get('student/details', 'StudentController@index')->name('student-details');
Route::post('student/create', 'StudentController@create');
Route::post('student/info', 'StudentController@getStudentInfo');

//Settings Routes
Route::get('user/profile', 'UserController@showProfile');
Route::get('settings/manage-users', 'UserController@manageUsers');
Route::post('user/profile/update-pass', 'UserController@setNewPassword');
Route::post('user/create', 'UserController@create');
Route::post('user/manage/role', 'UserController@setRole');

//Laboratory Routes
Route::get('laboratory/manage', 'LaboratoryController@manage');
Route::post('laboratory/create', 'LaboratoryController@create');
Route::post('laboratory/update', 'LaboratoryController@update');
Route::post('laboratory/info', 'LaboratoryController@showInfo');
Route::get('laboratory/view/{id}/status', 'LaboratoryController@show');
Route::get('laboratory/export/{lab_id}/users', 'LaboratoryController@exportLabUsers');

//Student Terminal Login
Route::post('student/terminal/login', 'StudentController@setTerminal');
Route::post('student/terminal/logout', 'StudentController@endTerminal');
Route::get('laboratory/terminal/{pc}/user', 'StudentController@getTerminal');
Route::post('student/terminal/logout/all', 'StudentController@endAllTerminal');

