<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class EntrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //create Arvin's admin account
        $arvin = User::create([
                'name' => 'John Arvin Nuarin',
                'lname' => 'Nuarin',
                'fname' => 'Arvin',
                'contact_no'=> '12345678901',
                'username'=> 'root',
                'email' => 'arvin@arvin.com',
                'password' => bcrypt('arvin2017'),
            ]); 

          //create a role of admin
            $admin = Role::create([
                'name' => 'admin',
                'display_name' => 'System Administrator',
                'description' => 'System Root Administrator',
            ]);

          //create a role for employee
          $emp = Role::create([
                'name' => 'emp',
                'display_name' => 'Employee',
                'description' => 'Informatics DMS Employee',
            ]);    

            //create a role for banned employee    
          $ban = Role::create([
                'name' => 'ban',
                'display_name' => 'Banned',
                'description' => 'Informatics DMS Banned Employee',
          ]);  

          $arvin->attachRole($admin); 

      }
}
