<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->integer('terminal_id')->unsigned();

            $table->foreign('student_id')->references('id')->on('student_info')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('terminal_id')->references('id')->on('terminal')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->string('attnd_status',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance');
    }
}
