<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('terminal_id')->unsigned();
            $table->foreign('terminal_id')->references('id')->on('terminal')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('asset_code')->unique();
            $table->string('rfid_code')->unique();
            $table->string('brand', 50);
            $table->string('model', 50);
            $table->string('serial_no', 50)->unique();
            $table->string('type', 50);
            $table->string('status', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment');
    }
}
