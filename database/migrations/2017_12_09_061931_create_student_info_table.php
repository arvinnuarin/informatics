<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_no',50)->unique();
            $table->string('fname',50);
            $table->string('lname',50);
            $table->string('mname',50)->nullable();
            $table->string('level',20);
            $table->string('course',200);
            $table->string('grade',20)->nullable();
            $table->string('section',20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_info');
    }
}
