<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pc_name',40)->unique();
            $table->string('ip_address',15)->unique();
            $table->integer('lab_id')->unsigned();

            $table->foreign('lab_id')->references('id')->on('laboratory')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->string('status', 40);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminal');
    }
}
