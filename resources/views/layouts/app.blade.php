<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#3c8dbc">
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}" type="image/png" />
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" type="text/css" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>IDMS @yield('htmltitle', 'IDMS') </title>
</head>
<body class="widescreen">
    <div id="app">

        @yield('content')
    </div>

    <script type="text/javascript" src="{{ mix('/js/app.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
    </script>
    <script type="text/javascript" src="{{asset('js/notifications.js')}}"></script>
    @yield('page-script')  
</body>
</html>
