@extends('layouts.dashboard.main')
@section('htmltitle')
	PC Terminals
@endsection

@section('pagetitle')
	PC Terminals
@endsection

@section('main-content')
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">

				<div class="pull-right">
					<button type="button" id="btnAddTerminal" class="btn btn-info waves-effect waves-light"><i class="fa fa-desktop"></i><i class="fa fa-plus"></i> Add Terminal PC</button>
				</div>

        <div class="col-md-12" style="margin-top: 30px">
          <div class="panel panel-color panel-dark"> 
            <div class="panel-heading"><h3 class="panel-title">PC Terminal List</h3></div>
            <div class="panel-body">
              <table class="table table-bordered table-striped data-table">
                <thead>
                  <tr>
                    <th>PC No.</th>
                    <th>Name</th>
                    <th>IP Address</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($terminals as $pc)
                    <tr>
                      <td>{{$pc->id}}</td>
                      <td>{{$pc->pc_name}}</td>
                      <td>{{$pc->ip_address}}</td>
                      <td>{{$pc->location}}</td>
                      <td>
                        @if($pc->status == 'ONLINE')
                        <span class="label label-success">
                        @else
                        <span class="label label-danger">
                        @endif
                        {{$pc->status}}</span></td>
                      <td><button type="button" class="btn btn-dark waves-effect waves-light btnTermUpdate" id="{{$pc->id}}"><i class="fa fa-desktop"></i><i class="fa fa-pencil"></i> Update</button></td>
                    </tr>

                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>PC No.</th>
                    <th>Name</th>
                    <th>IP Address</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>	
</div>

@include('layouts.terminal.terminal')

@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/terminals.js')}}"></script>
@endsection
