<!--Terminal Modal-->
<div class="modal fade" id="mdlTerminal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="mdlbl">Add a New Terminal</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" role="form" id="frmMdlTerminal">
      		<div class="form-group">
      			<label class="col-md-4 control-label">PC Terminal Name</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input PC Terminal Name" name="pc_name">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-md-4 control-label">PC IPv4 Address</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input PC Terminal IPv4 Address" name="pc_ipaddress">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-md-4 control-label">PC Terminal Location</label>
      			<div class="col-md-8"> 
      				<select class="form-control" id="cmbTermLoc" name="pc_location">
                @foreach($locs as $loc)
                  <option value="{{$loc->id}}" selected>{{$loc->name}}</option>
                @endforeach
              </select>
      			</div>
      		</div>
          <div class="form-group">
            <label class="col-md-4 control-label">Status</label>
            <div class="col-md-8">
              <select class="form-control" id="cmbTermStatus" name="status">
                  <option value="ONLINE">ONLINE</option>
                  <option value="OFFLINE">OFFLINE</option>
              </select>
            </div>
          </div>
          <input type="text" name="shouldUpdate"  value="granted" hidden>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnMdlTermSubmit">Save</button>
      </div>
    </div>
  </div>
</div>
<!--end modal-->