@extends('layouts.dashboard.main')
@section('htmltitle')
	Student Information
@endsection

@section('pagetitle')
	Student Details
@endsection

@section('main-content')
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">

        <div class="col-md-12">
          <div class="pull-right">
            <button type="button" id="btnAddStudent" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-account-plus"> </i>Add Student</button>
          </div>
        </div>
        
        <div class="col-md-12" style="margin-top: 30px">
          <div class="panel panel-color panel-dark"> 
            <div class="panel-heading"><h3 class="panel-title">Student List</h3></div>
            <div class="panel-body">

              <ul class="nav nav-tabs navtab-bg">
                <li class="active">
                  <a href="#college" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">College</span>
                  </a>
                </li>
                <li class=""> 
                  <a href="#high-school" data-toggle="tab" aria-expanded="false">
                  <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">High School</span>
                  </a>
                </li>
              </ul>

              <div class="tab-content">
                <div class="tab-pane active" id="college">
                  <table class="table table-bordered table-striped data-table">
                    <thead>
                      <tr>
                        <th>Student No.</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Course</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($college as $col)
                        <tr>
                          <td>{{$col->student_no}}</td>
                          <td>{{$col->lname}}</td>
                          <td>{{$col->fname}}</td>
                          <td>{{$col->mname}}</td>
                          <td>{{$col->course}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Student No.</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Course</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="tab-pane" id="high-school">
                  <table class="table table-bordered table-striped data-table">
                    <thead>
                      <tr>
                        <th>Student No.</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Grade</th>
                        <th>Section</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($high as $hs)
                        <tr>
                          <td>{{$hs->student_no}}</td>
                          <td>{{$hs->lname}}</td>
                          <td>{{$hs->fname}}</td>
                          <td>{{$hs->mname}}</td>
                          <td>{{$hs->grade}}</td>
                          <td>{{$hs->section}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Student No.</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Grade</th>
                        <th>Section</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>	
</div>

@include('layouts.student.student')
@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/students.js')}}"></script>
@endsection
