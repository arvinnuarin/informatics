<!--student Modal-->
<div class="modal fade" id="mdlStudent" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="mdlbl">Add a New Student</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" role="form" id="frmMdlStudent">
      		<div class="form-group">
      			<label class="col-md-4 control-label">Student Number</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Student Number" name="stud_number">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-md-4 control-label">Last Name</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Student Last Name" name="last_name">
      			</div>
      		</div>
          <div class="form-group">
            <label class="col-md-4 control-label">First Name</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Student First Name" name="first_name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Middle Name</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Student Middle Name" name="middle_name">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Level</label>
            <div class="col-md-8">
              <select class="form-control" id="cmbStudLevel" name="level">
                  <option value="COLLEGE" selected>College</option>
                  <option value="HIGH SCHOOL">High School</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" id="lblCourse">Course</label>
            <div class="col-md-8">
              <select class="form-control" name="course" id="cmbStudCourse">
              </select>
            </div>
          </div>
          <div id="hsect">
              <div class="form-group">
              <label class="col-md-4 control-label">Grade</label>
              <div class="col-md-8">
                <select class="form-control" name="grade" id="cmbStudGrade">
                  <option value="GRADE 11">GRADE 11</option>
                  <option value="GRADE 12">GRADE 12</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="sect">
              <label class="col-md-4 control-label">Section</label>
              <div class="col-md-8">
                <select class="form-control" name="section" id="cmbStudSection">
                </select>
              </div>
            </div>
          </div>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnMdlStudSubmit">Save</button>
      </div>
    </div>
  </div>
</div>
<!--end modal-->