@extends('layouts.dashboard.main')
@section('htmltitle')
	User Profile
@endsection

@section('pagetitle')
	User Profile
@endsection

@section('main-content')

  <!--My Profile-->
  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h4 class="m-t-0">My Profile</h4>
          
          
      </div>
    </div>
  </div>

  <!--Update Password-->
  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h4 class="m-t-0">Update Password</h4>
        <form class="form-horizontal" role="form" id="frmUpdatePass">
          <div class="form-group">
            <label class="col-md-4 control-label">Current Password</label>
            <div class="col-md-8"> 
              <input type="password" class="form-control" placeholder="Input Current Password" name="cur_pass" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">New Password</label>
            <div class="col-md-8"> 
              <input type="password" class="form-control" placeholder="Input New Password" name="new_pass" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Confirm New Password</label>
            <div class="col-md-8"> 
              <input type="password" class="form-control" placeholder="Confirm New Password" name="new_pass_confirmation" required>
            </div>
          </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="btUpdatePass">Update Password</button>
      </div>
      </div>
    </div>
  </div

@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/settings.js')}}"></script>
@endsection
