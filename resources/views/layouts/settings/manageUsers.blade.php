@extends('layouts.dashboard.main')
@section('htmltitle')
	System Users
@endsection

@section('pagetitle')
	Manage Users
@endsection

@section('main-content')
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">

				<div class="pull-right">
					<button type="button" id="btnAddUser" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-account-plus"></i> Add New User</button>
				</div>

        <div class="col-md-12" style="margin-top: 30px">
          <div class="panel panel-color panel-dark"> 
            <div class="panel-heading"><h3 class="panel-title">System User List</h3></div>
            <div class="panel-body">
              <table class="table table-bordered table-striped data-table">
                <thead>
                  <tr>
                    <th>ID No.</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Email Address</th>
                    <th>Contact No.</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                    <tr>
                      <td>{{$user->id}}</td>
                      <td>{{$user->lname}}</td>
                      <td>{{$user->fname}}</td>
                      <td>{{$user->mname}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->contact_no}}</td>

                      <td><span class="label label-primary">{{$user->roles[0]->display_name}}</span></td>
                      <td>
                        @if($user->roles[0]->display_name == 'Employee')
                        <button id="{{$user->id}}" class="btn btn-danger waves-effect waves-light btnUserAction" status="emp"><i class="fa fa-lock"></i> Block User</button>
                        @elseif($user->roles[0]->display_name == 'Banned')
                        <button id="{{$user->id}}" class="btn btn-warning waves-effect waves-light btnUserAction" status="ban"><i class="fa fa-unlock"></i> Unblock User</button>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID No.</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Email Address</th>
                    <th>Contact No.</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
			</div>
		</div>
	</div>	
</div>


<!--terminal Modal-->
<div class="modal fade" id="mdlUser" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="mdlbl">Add a New User</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" role="form" id="frmMdlUser">
      		<div class="form-group">
            <label class="col-md-4 control-label">Username</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Employee desired Username" name="username" required>
            </div>
          </div>
          <div class="form-group">
      			<label class="col-md-4 control-label">Last Name</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Employee Last Name" name="lname" required>
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-md-4 control-label">First Name</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Employee First Name" name="fname" required>
      			</div>
      		</div>
          <div class="form-group">
            <label class="col-md-4 control-label">Middle Name</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Employee Middle Name" name="mname" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Email Address</label>
            <div class="col-md-8"> 
              <input type="email" class="form-control" placeholder="Input Employee Email Address" name="email" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Contact No.</label>
            <div class="col-md-8"> 
              <input type="number" class="form-control" placeholder="Input Employee Contact Number" name="contact" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Default Password</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" value="secret2017" readonly>
            </div>
          </div>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnMdlUserSubmit">Create</button>
      </div>
    </div>
  </div>
</div>
<!--end modal-->
@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/settings.js')}}"></script>
@endsection
