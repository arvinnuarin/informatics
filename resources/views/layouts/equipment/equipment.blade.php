<!--New Equipment Modal-->
<div class="modal fade" id="mdlEquipment" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="mdlbl">Add a New Equipment</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" role="form" id="frmMdlEquipment">
      		<div class="form-group">
      			<label class="col-md-4 control-label">Asset Code</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Equipment Asset Code" name="asset_code">
      			</div>
      		</div>
          <div class="form-group">
            <label class="col-md-4 control-label">RFID Code</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Equipment RFID Code" name="rfid_code">
            </div>
          </div>
      		<div class="form-group">
      			<label class="col-md-4 control-label">Brand</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Equipment Brand name" name="brand">
      			</div>
      		</div>
          <div class="form-group">
            <label class="col-md-4 control-label">Model</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Equipment Model name" name="model">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Serial Number</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Equipment Serial No." name="serial_no">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Type</label>
            <div class="col-md-8">
              <select class="form-control" id="cmbEquipType" name="type">
                  <option value="MOUSE">MOUSE</option>
                  <option value="MONITOR">MONITOR</option>
                  <option value="KEYBOARD">KEYBOARD</option>
                  <option value="AVR">AVR</option>
                  <option value="OTHERS">OTHERS</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Status</label>
            <div class="col-md-8">
              <select class="form-control" id="cmbEquipStatus" name="status">
                  <option value="OPERATIONAL">OPERATIONAL</option>
                  <option value="MAINTENANCE">MAINTENANCE</option>
                  <option value="LOST">LOST</option>
                  <option value="UNOPERATIONAL">UNOPERATIONAL</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Connected to</label>
            <div class="col-md-8"> 
              <select class="form-control" id="cmbEquipPC" name="connected_pc">
                  @foreach($pcs as $pc)
                    <option value="{{$pc->id}}">{{$pc->pc_name.' ('.$pc->ip_address.')'}}</option>
                  @endforeach
              </select>
            </div>
          </div>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnMdlEquipSubmit">Create</button>
         <button type="button" class="btn btn-primary" id="btnMdlEquipUpdate" style="display: none">Update</button>
      </div>
    </div>
  </div>
</div>