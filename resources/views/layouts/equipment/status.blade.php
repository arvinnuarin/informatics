@extends('layouts.dashboard.main')
@section('htmltitle')
	Equipment Status
@endsection

@section('pagetitle')
	Equipment Status
@endsection

@section('main-content')
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">

				<div class="pull-right">
					<button type="button" id="btnAddEquipment" class="btn btn-info waves-effect waves-light"><i class="fa fa-keyboard-o"></i> Add Equipment</button>
				</div>

				<div class="col-md-12" style="margin-top: 30px">
		          <div class="panel panel-color panel-dark"> 
		            <div class="panel-heading"><h3 class="panel-title">Equipment List</h3></div>
		            <div class="panel-body">
		              <table class="table table-bordered table-striped data-table">
		                <thead>
		                  <tr>
		                    <th>PC Name</th>
		                    <th>Asset Code</th>
		                    <th>RFID Code</th>
		                    <th>Serial No.</th>
		                    <th>Brand</th>
		                    <th>Model</th>
		                    <th>Type</th>
		                    <th>Status</th>
		                    <th>Action</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  @foreach($equipments as $equip)
		                    <tr>
		                      <td><strong>{{$equip->pc_name}}</strong></td>
		                      <td>{{$equip->asset_code}}</td>
		                      <td>{{$equip->rfid_code}}</td>
		                      <td>{{$equip->serial_no}}</td>
		                      <td>{{$equip->brand}}</td>
		                      <td>{{$equip->model}}</td>
		                      <td>{{$equip->type}}</td>
		                      <td><strong>{{$equip->status}}</strong></td>
		                      <td><button type="button" class="btn btn-success waves-effect waves-light btnEquipUpdate" id="{{$equip->id}}">Update</button></td>
		                    </tr>
		                  @endforeach
		                </tbody>
		                <tfoot>
		                  <tr>
		                    <th>PC Name</th>
		                    <th>Asset Code</th>
		                    <th>RFID Code</th>
		                    <th>Serial No.</th>
		                    <th>Brand</th>
		                    <th>Model</th>
		                    <th>Type</th>
		                    <th>Status</th>
		                    <th>Action</th>
		                  </tr>
		                </tfoot>
		              </table>
		            </div>
		          </div>
        		</div>
			</div>
		</div>
	</div>
</div>

@include('layouts.equipment.equipment')

@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/equipments.js')}}"></script>
@endsection
