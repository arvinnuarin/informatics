@extends('layouts.dashboard.main')
@section('htmltitle')
	Equipment Inventory
@endsection

@section('pagetitle')
	Equipment Inventory
@endsection

@section('main-content')
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">

				<div class="pull-right">
					<button type="button" id="btnAddEquipment" class="btn btn-info waves-effect waves-light"><i class="fa fa-keyboard-o"></i> Add Equipment</button>
				</div>

				<div class="col-md-12" style="margin-top: 30px">
		          <div class="panel panel-color panel-dark"> 
		            <div class="panel-heading"><h3 class="panel-title">Equipment Inventory Status List</h3></div>
		            <div class="panel-body">
		              <table class="table table-bordered table-striped data-table">
		                <thead>
		                  <tr>
		                  	<th>Type</th>
		                    <th>Brand</th>
		                    <th>Model</th>
		                    <th>Total</th>
		                    <th>Operational</th>
		                    <th>Unoperational</th>
		                    <th>Maintenance</th>
		                    <th>Lost / Stolen</th>
		                  </tr>
		                </thead>
		                <tbody>
		                 @foreach($inventory as $in)
		                 	<tr>
		                 		<td>{{$in->type}}</td>
		                 		<td>{{$in->brand}}</td>
		                 		<td>{{$in->model}}</td>
		                 		<td><strong>{{$in->total}}</strong></td>
		                 		<td class="text-success"><strong>{{$in->operational}}</strong></td>
		                 		<td class="text-danger"><strong>{{$in->unoperational}}</strong></td>
		                 		<td class="text-info"><strong>{{$in->maintenance}}</strong></td>
		                 		<td class="text-warning"><strong>{{$in->lost}}</strong></td>
		                 	</tr>
		                 @endforeach
		                </tbody>
		                <tfoot>
		                  <tr>
		                  	<th>Type</th>
		                    <th>Brand</th>
		                    <th>Model</th>
		                    <th>Total</th>
		                    <th>Operational</th>
		                    <th>Unoperational</th>
		                    <th>Maintenance</th>
		                    <th>Lost / Stolen</th>
		                  </tr>
		                </tfoot>
		              </table>
		            </div>
		          </div>
        		</div>
			</div>
		</div>
	</div>
</div>

@include('layouts.equipment.equipment')

@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/equipments.js')}}"></script>
@endsection