@extends('layouts.dashboard.main')
@section('htmltitle')
	Notification Center
@endsection

@section('pagetitle')
	Notification Center
@endsection

@section('main-content')

<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">
				<div class="list-group">

 				@foreach($notifs as $notif)

 					@if($notif->type == "info")
 					<div class="alert alert-info alert-dismissible fade in">
 					@elseif($notif->type == 'success')
 					 <div class="alert alert-success alert-dismissible fade in">
 					@elseif ($notif->type == 'warning')
 					 <div class="alert alert-warning alert-dismissible fade in">
 					@else
 					<div class="alert alert-danger alert-dismissible fade in"> 
 					@endif
						<a href="{{'/'.$notif->url_link}}" class="alert-link">
						<i class="fa fa-info-circle"></i>
					    {{$notif->name. ' @ '.$notif->created_at.' : '.$notif->message}}</a>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
					</div>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page-script')
<script type="text/javascript">
	
//reload page every 5 seconds
setTimeout(function() {
    location.reload();
}, 5000);

</script>
@endsection