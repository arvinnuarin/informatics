<div class="topbar">
    <!--logo-->
    <div class="topbar-left">
        <div class="text-center"> <a href="{{route('home')}}" class="logo">IDMS</a><a href="{{route('home')}}" class="logo-sm"><span>ID</span></a></div>
    </div>
    <!--top bar-->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                            <button type="button" class="button-menu-mobile open-left waves-effect float-icon-light"> <i class="mdi mdi-menu"></i> </button> <span class="clearfix"></span></div>
                <ul class="nav navbar-nav navbar-right pull-right">
                     @role(['admin','emp'])
                    <!--Notification Bell-->
                    <li class="dropdown hidden-xs"> <a href="#" class="dropdown-toggle waves-effect waves-light notification-icon-box" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-bell"></i> <span class="badge badge-xs badge-danger"></span> </a>
                        <ul class="dropdown-menu dropdown-menu-lg">
                            <li class="text-center notifi-title">Notification <span class="badge badge-xs badge-success" id="notifi-count"></span></li>
                                <li class="list-group" id="notifi-list">
                                    <a href="{{url('/notification/center')}}" class="list-group-item"> <small class="text-primary">See all notifications</small> </a></li>
                            </ul>
                        </li>
                        @endrole
                    <!--Fullscreen button-->
                    <li class="hidden-xs"> <a href="#" id="btn-fullscreen" class="waves-effect waves-light notification-icon-box"><i class="mdi mdi-fullscreen"></i></a></li>
                    <!--Profile-->
                    <li class="dropdown"> <a href="#" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"> <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="user-img" class="img-circle"> <span class="profile-username">{{Auth::user()->name}}<br/> <small>{{ Auth::user()->roles[0]->display_name}}</small> </span> </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('user/profile')}}"> Profile</a></li>
                            <li><a href="{{url('user/settings')}}"> Settings </a></li>
                            <li><a href="{{route('logout')}}"> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>