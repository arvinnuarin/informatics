<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <!--Home-->
                @role(['admin','emp'])
                <li> <a href="{{route('home')}}" class="waves-effect"><i class="mdi mdi-home"></i><span> Dashboard</a></li>
                    <!--Notification-->
                <li> <a href="{{url('notification/center')}}" class="waves-effect"><i class="fa fa-bell"></i><span> Notification</a></li>
                @endrole
                <!--Laboratories-->
                <li> <a href="{{url('laboratory/manage')}}" class="waves-effect"><i class="fa fa-building-o"></i><span> Laboratory</a></li>
                
                @role(['admin','emp'])
                <!--PC Terminal-->
                <li> <a href="{{route('terminal')}}" class="waves-effect"><i class="fa fa-desktop"></i><span> PC Terminal</a></li>
                <!--Equipment-->
                <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-keyboard-o"></i> <span> Equipment </span> <span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{url('equipment/inventory')}}"><i class="fa fa-tag"></i> Parts Inventory</a></li>
                        <li><a href="{{url('equipment/status')}}"><i class="fa fa-info-circle"></i> Parts Status</a></li>
                    </ul>
                </li>
                <!--Student-->
                <li><a href="{{route('student-details')}}" class="waves-effect"><i class="mdi mdi-account-multiple"></i><span> Student Details</a></li>
                @endrole

                <!--Settings-->
                <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-wrench"></i> <span> Settings </span> <span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{url('user/profile')}}"><i class="fa fa-key"></i> User Profile</a></li>
                        @role('admin')
                        <li><a href="{{url('settings/manage-users')}}"><i class="fa fa-user-circle"></i> Manage Users</a></li>
                        @endrole
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>