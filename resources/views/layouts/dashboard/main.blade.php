@extends('layouts.app')
@section('content')
    <div id="wrapper" class="forced">
	@include('layouts.dashboard.topbar')
	@include('layouts.dashboard.sidebar')

	<div class="content-page">
        
        <div class="content">
            <div class="">
            @role(['admin', 'emp','student'])
                <div class="page-header-title">
                    <h4 class="page-title">@yield('pagetitle')</h4></div>
            </div>
             <div class="page-content-wrapper ">
                <div class="container">
                    <div class="row">
                    	@yield('main-content')
                    </div>
                </div>
            </div>
            @endrole

            @role('ban')
            <div class="page-header-title">
                    <h4 class="page-title">Sorry! Your account has been blocked. Please contact system administrator.</h4></div>
            </div>
            @endrole
        </div>
         
    </div>

	@include('layouts.dashboard.footer')
    </div>
@endsection
