<!--New Laboratory Modal-->
<div class="modal fade" id="mdlLaboratory" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="mdlbl">Add a New Laboratory</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" role="form" id="frmMdlLab">
      		<div class="form-group">
      			<label class="col-md-4 control-label">Name</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Laboratory Name" name="lab_name">
      			</div>
      		</div>
          <div class="form-group">
            <label class="col-md-4 control-label">Faculty Name</label>
            <div class="col-md-8"> 
              <input type="text" class="form-control" placeholder="Input Faculty in-charge" name="lab_faculty">
            </div>
          </div>
      		<div class="form-group">
      			<label class="col-md-4 control-label">Subject</label>
      			<div class="col-md-8"> 
      				<input type="text" class="form-control" placeholder="Input Subject Name" name="lab_subject">
      			</div>
      		</div>
      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnMdlLabSubmit">Create</button>
         <button type="button" class="btn btn-primary" id="btnMdlLabUpdate" style="display: none">Update</button>
      </div>
    </div>
  </div>
</div>