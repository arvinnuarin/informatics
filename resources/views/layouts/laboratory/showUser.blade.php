@extends('layouts.dashboard.main')
@section('htmltitle')
	{{$info->pc_name. ' User Details'}}
@endsection

@section('pagetitle')
	{{$info->pc_name. ' User Details'}}
@endsection

@section('main-content')

<div class="col-lg-4">
	<div class="panel panel-color panel-info">
		<div class="panel-heading"> 
			<h3 class="panel-title">{{$info->pc_name. ' Student User'}}</h3> 
		</div>
		<div class="panel-body text-center">
			<div class="col-xs-offset-3">
				<img class="img-responsive img-circle" src="{{url('/assets/images/users/student.png')}}" alt="User" width="200">
			</div>
			<h2>{{$info->fname.' '.$info->lname}}</h2>
			<h3>{{$info->student_no}}</h3>
			<h3>{{$info->course}}</h3>
		</div>
	</div>
</div>

<div class="col-lg-4">
	<div class="panel panel-color panel-success">
		<div class="panel-heading"> 
			<h3 class="panel-title">Terminal PC Details</h3> 
		</div>
		<div class="panel-body">
			<h3>PC Name: <span class="label label-primary">{{$info->pc_name}}</span></h3>
			<h3>PC IP Add: <span class="label label-danger">{{$info->ip_address}}</span></h3>
			<h3>PC Status: <span class="label label-info">{{$info->status}}</span></h3>
		</div>
	</div>
</div>

<div class="col-lg-4">
	<div class="panel panel-color panel-primary">
		<div class="panel-heading clearfix"> 
			<h3 class="panel-title pull-left">Session Details</h3>
			@role('student')
				@if($info->pcid == $studpc)
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-danger waves-effect waves-light btnTerminate" id="{{$info->term_id}}"><i class="fa fa-power-off"></i> Terminate</button>
					</div>
				@endif
			@endrole
		</div>

		<div class="panel-body text-center">
			<h3>Login Time: <span class="label label-info">{{$info->created_at}}</span></h3>
		</div>			
	</div>
</div>
@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/laboratory.js')}}"></script>
@endsection