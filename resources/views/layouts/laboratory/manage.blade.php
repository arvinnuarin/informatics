@extends('layouts.dashboard.main')
@section('htmltitle')
	Laboratories Info
@endsection

@section('pagetitle')
	Laboratories Information
@endsection

@section('main-content')
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
</style>
<div class="col-lg-12">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">
				@role(['admin','emp'])
				<div class="pull-right">
					<button type="button" id="btnAddLaboratory" class="btn btn-info waves-effect waves-light"><i class="fa fa-keyboard-o"></i> Add Laboratory</button>
				</div>
				@endrole

				<div class="col-md-12" style="margin-top: 30px">
		          <div class="panel panel-color panel-dark"> 
		            <div class="panel-heading"><h3 class="panel-title">Laboratory List</h3></div>
		            <div class="panel-body">
		              <table class="table table-bordered table-striped data-table">
		                <thead>
		                  <tr>
		                    <th>Name</th>
		                    <th>Faculty</th>
		                    <th>Subject</th>
		                    <th>PC Total</th>
		                    <th>Action</th>
		                   </tr>
		                </thead>
		                <tbody>
		                	@foreach($labs as $lab)
		                	<tr>
		                 		<td><strong>{{$lab->name}}</strong></td>
		                 		<td>{{$lab->faculty}}</td>
		                 		<td>{{$lab->subject}}</td>
		                 		<td><strong>{{$lab->pc_nos}}</strong></td>
		                 		<td>
		                 			<div class="btn-group-vertical">
		                 				@role(['admin','emp'])
		                 				<button type="button" class="btn btn-dark waves-effect waves-light btnLabUpdate" id="{{$lab->id}}"><i class="fa fa-pencil"></i>  Update</button>
		                 				@endrole
		                 				<button type="button" class="btn btn-primary waves-effect waves-light btnLabView" id="{{$lab->id}}"><i class="fa fa-info-circle"></i> Status</button>
		                 			</div>
		                 		</td>
		                 	</tr>
		                	@endforeach
		                </tbody>
		                <tfoot>
		                  <tr>
		                    <th>Name</th>
		                    <th>Faculty</th>
		                    <th>Subject</th>
		                    <th>PC Total</th>
		                    <th>Action</th>
		                   </tr>
		                </tfoot>
		              </table>
		            </div>
		          </div>
        		</div>
			</div>
		</div>
	</div>
</div>

@include('layouts.laboratory.newLab')

@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/laboratory.js')}}"></script>
@endsection