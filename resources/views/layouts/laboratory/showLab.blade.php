@extends('layouts.dashboard.main')
@section('htmltitle')
	{{$labinfo[0]->name}}
@endsection

@section('pagetitle')
	{{$labinfo[0]->name.' Details'}}
@endsection

@section('main-content')

<div class="col-lg-6">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">
				<h4>Faculty in charge: <span class="label label-primary">{{$labinfo[0]->faculty}}</span></h4>
				<h4>Subject: <span class="label label-info">{{$labinfo[0]->subject}}</span></h4>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-6">
	<div class="panel-primary panel">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-8">
					<h4>ONLINE PC: <span style="margin-left: 7px"></span><span class="label label-info">{{$online.' PC'}}</span></h4>
				    <h4>OFFLINE PC: <span class="label label-danger">{{$offline.' PC'}}</span></h4>
				</div>
				<div class="col-lg-4">
					@role(['admin','emp'])
					<div class="pull-right btn-group-vertical">
						<button type="button" class="btn btn-danger btnClearAll" id="{{$labinfo[0]->lab_id}}"><i class="fa fa-power-off"></i> Clear All Session</button>
							
					 	<button type="button" class="btn btn-info btnLabExport" id="{{$labinfo[0]->lab_id}}"><i class="fa fa-file-pdf-o"></i> PDF Report</button>
					</div>
					@endrole
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="col-lg-12">
@foreach($labinfo as $pc)
	<div class="col-lg-2">
		@if($pc->status == 'ONLINE')
			@if($pc->id == $studpc)
				<div class="panel panel-color panel-warning">
			@else 
				<div class="panel panel-color panel-primary">
			@endif
				<div class="panel-heading"> 
					<h3 class="panel-title">{{'PC-'.$pc->id.': '.$pc->status}}</h3> 
				</div>

				<div class="panel-body text-center">
					<p><strong>{{$pc->fname.' '.$pc->lname}}</strong></p>
					<button type="button" class="btn btn-primary waves-effect waves-light btnDetails" id="{{$pc->id}}">Details</button>
				</div>
			</div>
		@else
		<div class="panel panel-color panel-danger"> 
			<div class="panel-heading"> 
				<h3 class="panel-title">{{'PC-'.$pc->id.': '.$pc->status}}</h3> 
			</div>
			<div class="panel-body text-center">
				
				@if($studpc <=0)
					<p><strong>PC AVAILABLE</strong></p>
					@role('student')
					<button type="button" class="btn btn-primary waves-effect waves-light btnStudLogin" id="{{$pc->id}}">Login</button>
					@endrole
				@else
					<p><strong>RESERVED</strong></p>
					<button type="button" class="btn btn-primary waves-effect waves-light btnReserved" disabled>Reserved</button>
				@endif

				@role(['admin','emp'])
				<button type="button" class="btn btn-primary waves-effect waves-light btnReserved" disabled>Reserved</button>
				@endrole

			</div>
		</div>
		@endif
	</div>
@endforeach
</div>
@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/laboratory.js')}}"></script>
<script type="text/javascript">
	
//reload page every 5 seconds
setTimeout(function() {
    location.reload();
}, 5000);
</script>
@endsection
