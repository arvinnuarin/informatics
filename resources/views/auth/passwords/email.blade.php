@extends('layouts.app')
@section('content')
<div class="container accountbg">
    <div class="wrapper-page">
        <div class="panel panel-color panel-primary panel-pages">
            <div class="panel-body">
                <h3 class="text-center m-t-0 m-b-15">Informatics Device Monitoring System</h3>
                <h4 class="text-muted text-center m-t-0">Reset Password</h4>
                <form class="form-horizontal m-t-20" method="POST" action="{{ route('password.email') }}" >
                    {{ csrf_field() }}

                    @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @elseif ($errors->has('password'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email Address" name="email">
                        </div>
                    </div>

                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Send Password Reset Link</button>
                        </div>
                    </div>
                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-7"> <a href="{{route('login')}}" class="text-muted"><i class="fa fa-user m-r-5"></i>Account Login</a></div>
                   </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
