@extends('layouts.app')
@section('htmltitle')
    Login
@endsection
@section('content')
<div class="container accountbg">
    <div class="wrapper-page">
        <div class="panel panel-color panel-primary panel-pages">
            <div class="panel-body">
                <h3 class="text-center m-t-0 m-b-15">Informatics Device Monitoring System</h3>
                <h4 class="text-muted text-center m-t-0">Sign in to start a session</h4>
                <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}" >
                    {{ csrf_field() }}

                    @if ($errors->has('username'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('username') }}</strong>
                        </div>
                    @elseif ($errors->has('password'))
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username" name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" placeholder="Password" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-7"> <a href="{{route('password.request')}}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a></div>
                   </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
