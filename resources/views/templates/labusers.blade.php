<!DOCTYPE html>
<html>
<head> 
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('/css/app.css')}}" rel="stylesheet" type="text/css" />
	<title>{{'Laboratory '.$datas[0]->name. ' Users'}}</title>
</head>
<body>
<style type="text/css">
  td, th {
    text-align: center;
    vertical-align: middle;
}
tr {
    	page-break-inside: avoid;
	}
</style>

<div class="container">
	<div class="row">

		<div class="header text-center">
		<img src="{{url('assets/images/informatics.png')}}"><br><br>
		<label>380 Rizal Avenue Extension Corner 11th Avenuegrace Park, Metro Manila</label><br>
		<label>63-02-363-5178 | +63-02-365-2361</label><br>
		<label>Info.Caloocan@informatics.com.ph</label>
		</div><br>

		<div class="col-xs-8">
			<h4>{{'LABORATORY '.$datas[0]->name. ' USER DETAILS'}}</h4>
			<h4>{{$datas[0]->subject}}</h4>
			<h4>{{$datas[0]->faculty}}</h4>
		</div>

		<div class="col-xs-4">
			<div class="pull-right">
				<h4>Date: <strong>{{\Carbon\Carbon::now()->format('F d, Y')}}</strong></h4>
			</div>
		</div>
	

		<div class="col-xs-12" style="margin-top: 2em"> 
 			<table class="table table-bordered table-striped">
                <thead>
                  <tr>
                  	<th>PC No.</th>
                    <th>PC Name</th>
                    <th>Student No</th>
                    <th>Student Name</th>
                    <th>Course</th>
                    <th>Login Time</th>
                  </tr>
                </thead>
                <tbody>
                	@foreach($datas as $pc)
                	<tr>
                		<td>{{'PC-'.$pc->id}}</td>
                		<td>{{$pc->pc_name}}</td>
                		<td>{{$pc->student_no}}</td>
                		<td>{{$pc->fname. ' '.$pc->lname}}</td>
                		<td>{{$pc->course}}</td>
                		<td>{{$pc->created_at}}</td>
                	</tr>
                	@endforeach
                </tbody>
            </table>
		</div>

		<div class="col-xs-12">
			
			<h4>Prepared by: <strong>INFORMATICS DMS</strong></h4>
			<p>Date Printed: {{\Carbon\Carbon::now()}}</p>
			
		</div>
		

	</div>
</div>

</body>
</html>