@extends('layouts.dashboard.main')
@section('htmltitle')
	Home Dashboard
@endsection

@section('pagetitle')
	Dashboard
@endsection

@section('main-content')

<!--PC Terminal #-->
<div class="col-sm-6 col-lg-3">    
    <div class="panel text-center">
        <div class="panel-heading">
            <h4 class="panel-title text-muted font-light">Total PC Terminals</h4></div>
        <div class="panel-body p-t-10">
            <h2 class="m-t-0 m-b-15"><i class="fa fa-desktop"></i> <b>{{$pc_no}}</b></h2>
            <p class="text-muted m-b-0 m-t-20"><a href="/terminal">Check Status</a></p>
        </div>
    </div>
</div>

<!--Students #-->
<div class="col-sm-6 col-lg-3">    
    <div class="panel text-center">
        <div class="panel-heading">
            <h4 class="panel-title text-muted font-light">Total Students</h4></div>
        <div class="panel-body p-t-10">
            <h2 class="m-t-0 m-b-15 text-info"><i class="mdi mdi-account-multiple"></i> <b>{{$student_no}}</b></h2>
            <p class="text-muted m-b-0 m-t-20"><a href="/student">Check Status</a></p>
        </div>
    </div>
</div>

<!--Equipment #-->
<div class="col-sm-6 col-lg-3">    
    <div class="panel text-center">
        <div class="panel-heading">
            <h4 class="panel-title text-muted font-light">Total Equipment</h4></div>
        <div class="panel-body p-t-10">
            <h2 class="m-t-0 m-b-15 text-success"><i class="fa fa-keyboard-o"></i> <b>{{$equip_no}}</b></h2>
            <p class="text-muted m-b-0 m-t-20"><a href="/equipment/status">Check Status</a></p>
        </div>
    </div>
</div>

<!--Active Terminal-->
<div class="col-sm-6 col-lg-3">    
    <div class="panel text-center">
        <div class="panel-heading">
            <h4 class="panel-title text-muted font-light">Active Terminal</h4></div>
        <div class="panel-body p-t-10">
            <h2 class="m-t-0 m-b-15 text-danger"><i class="fa fa-desktop"></i> <b>{{$active_pc}}</b></h2>
            <p class="text-muted m-b-0 m-t-20"><a href="/terminal">Check Status</a></p>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="panel panel-primary">
        <div class="panel-body">
            <h4 class="m-t-0">Equipment Inventory Status</h4>
            <canvas id="inventoryChart" height="auto" width="auto"></canvas>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="panel panel-primary">
        <div class="panel-body">
           <table class="table table-bordered table-striped data-table">
            <thead>
              <tr>
                <th>PC Name</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Type</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach($equipments as $equip)
                <tr>
                  <td><strong>{{$equip->pc_name}}</strong></td>
                  <td>{{$equip->brand}}</td>
                  <td>{{$equip->model}}</td>
                  <td>{{$equip->type}}</td>
                  <td><strong>{{$equip->status}}</strong></td>
                </tr>
              @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>


@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/dashboard.js')}}"></script>
@endsection