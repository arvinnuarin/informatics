$(document).ready(function(){

	//Add Terminal
	$('#btnAddTerminal').on('click', function(){

		$('#mdlTerminal').modal('show');
	});

	$('#btnMdlTermSubmit').on('click', function(){

		$.ajax({
		    type: "POST",
		    url: "/terminal/create",
		    data: $('#frmMdlTerminal').serialize(),
      			success: function(d) {
      				swal('Great!', 'A new terminal has been added','success');
      				$('#mdlTerminal').modal('hide');
      			},
      			error: function(d){
      				showRespErrors(d.responseJSON);
      			}
      		});
	});
});

function showRespErrors(data){

	var errorString ='<div><ul>';
	try{
		$.each(data.errors, function(key,value) {errorString += '<li>' + value + '</li>';});
		errorString+='</ul></div>';
	} catch(e){
		errorString = 'This IP address already exists.';
		data.message = 'IP Address Conflict';
	}
	
	swal(data.message,errorString,'error');
}