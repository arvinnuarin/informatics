
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');
require('jquery-slimscroll');
require('jquery.nicescroll');
require('jquery.scrollto');
require('fastclick');
require('./datatables');
require('./waves');
require('chart.js/dist/Chart.bundle.min');
require('./functions');
