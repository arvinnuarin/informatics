# Informatics Device Monitoring System

IDMS is a monitoring system designed for Informatics laboratory. This aims to deter and detect potential theft of its equipment using the RFID technology.

### Installation

IDMS requires NPM [Node.js](https://nodejs.org/) v4+, [XAMPP](https://www.apachefriends.org/download.html/), [Composer](https://getcomposer.org/download/), [Laravel 5.5](https://laravel.com/docs/5.5), [Git Bash](https://git-scm.com/downloads) to run.

1. Install the said applications and boot up the terminal.
    
    ```sh
    $ composer global require "laravel/installer"
    ```
2. Navigate to your XAMPP htdocs directory and clone this repository.

    ```sh
    $ git clone https://arvinnuarin@bitbucket.org/arvinnuarin/informatics.git
    $ cd informatics
    ```
3. Install all dependencies and compile it.
    ```sh
    $ composer install
    $ npm install
    $ npm run dev
    ```
4. Access your local **phpmyadmin** and create a database called **dbinformatics**. Then run this command to migrate all tables to the database.
     ```sh
    $ php artisan migrate
    $ php artisan db:seed
    ```
5. Boot up the server and navigate to this [site](http://localhost:8000).
     ```sh
    $ php artisan serve
    ```