$(document).ready(function(){

	initDataTable();
	setCourseValue("COLLEGE");
	setSectionValue("GRADE 11");

	//Add student
	$('#btnAddStudent').on('click', function(){

		$('#mdlStudent').modal('show');
	});

	$('#cmbStudLevel').on('change', function(){
		setCourseValue($(this).val());
	});

	$('#cmbStudGrade').on('change', function(){
		setSectionValue($(this).val());
	});

	$('#btnMdlStudSubmit').on('click', function(){

		axios.post('/student/create',$('#frmMdlStudent').serialize())
		.then(function(resp){
			swal('Great!', 'A new student has been added','success');
			sendNotification('Added a new student.','info', '');
			showToastr('A new student has been added.','success');
			$('#mdlStudent').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});
});

function setCourseValue(selVal){

	var courses = {
        "COLLEGE": {
            "course1": "BS Information Technology",
            "course2": "BS Computer Science",
            "course3": "BS Information Systems",
            "course4": "BS Business Administration",
            "course5": "BS Entrepreneurship"
        },
        "HIGH SCHOOL": {
            "strand1": "ABM",
            "strand2": "HUMSS",
            "strand3": "GAS",
            "strand4": "STEM"
        }
    };

    if(selVal == "COLLEGE") {
    	$("#lblCourse").text('Course');
    	$('#hsect').hide();
    } else {
    	$("#lblCourse").text('Strand');
    	$('#hsect').show();
    }

	$('#cmbStudCourse').empty().append(function() {
        
        var output = '';
		$.each(courses[selVal], function(k,v){
			output += '<option value="'+v+'">' + v + '</option>';
		});
        return output;
    });

    
}

function setSectionValue(sectVal){

    var sections = {
    	"GRADE 11": {
    		"section1": "Mac",
    		"section2": "Windows",
    		"section3": "Humms",
    		"section4": "Gas",
    		"section5": "Seekers",
    	},
    	"GRADE 12": {
    		"section1": "Bill Gates",
    		"section2": "Zuckerburg",
    		"section3": "Steve Jobs",
    		"section4": "Kootler"
    	}
    }

	$('#cmbStudSection').empty().append(function() {
        
        var output = '';
		$.each(sections[sectVal], function(k,v){
			output += '<option value="'+v+'">' + v + '</option>';
		});
        return output;
    });
}