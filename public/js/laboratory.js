$(document).ready(function(){

	initDataTable();
	var labID = 0;

	//Add Laboratory
	$('#btnAddLaboratory').on('click', function(){

		setModalValues('Add New Laboratory','','', '', '', '','MOUSE','OPERATIONAL','1',false);
	});

	//Create Laboratory
	$('#btnMdlLabSubmit').on('click', function(){

		axios.post('/laboratory/create',$('#frmMdlLab').serialize())
		.then(function(resp){
			swal('Great!', 'A new laboratory has been created.','success');
			sendNotification('Added a new laboratory.','info', '');
			showToastr('A new laboratory has been created.','success');
			$('#mdlLaboratory').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});

	//Show Laboratory Information
	$('.btnLabUpdate').on('click', function(){

		axios.post('/laboratory/info', {lab_id: $(this).prop('id')})
		.then(function(resp){
			var d = resp.data;
			setModalValues(d.name + ' Information',d.name,d.faculty, d.subject,true);
			labID = d.id;
		});
	});

	//Update Lab Info
	$('#btnMdlLabUpdate').on('click', function(){

		var data = $('#frmMdlLab').serializeArray(); // convert form to array
		data.push({name: "lab_id", value: labID});

		axios.post('/laboratory/update',$.param(data))
		.then(function(resp){
			swal('Great!', 'Laboratory Information has been updated.','success');
			sendNotification('Updated laboratory information.','info', '');
			showToastr('Laboratory Information has been updated.','success');
			$('#mdlLaboratory').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});

	//Show Laboratory Status
	$('.btnLabView').on('click', function(){

		window.open('/laboratory/view/'+$(this).prop('id')+'/status');
	});

	//Laboratory Student Login
	$('.btnStudLogin').on('click', function(){ //add attendance

		var termID = $(this).prop('id');
		axios.post('/student/terminal/login',{term_id: termID})
		.then(function(resp){
			swal('Great!', 'Your attendance has been recorded.','success');
			sendNotification('Login to PC-'+termID+'.','success', '');
			showToastr('Your attendance has been recorded.','success');
			$('#mdlLaboratory').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});

	$('.btnDetails').on('click', function(){ //show pc user

		window.open('/laboratory/terminal/'+$(this).prop('id')+'/user');
	});

	//Laboratory Terminal user
	$('.btnTerminate').on('click', function(){ //terminate session

		var term_id = $(this).prop('id');
		swal({	
			title: 'Are you sure?', text: "You want to end your session?",
			type: 'warning', showCancelButton: true, confirmButtonColor: '#3085d6',
  			cancelButtonColor: '#d33', confirmButtonText: 'Yes, I confirm!'
		}).then(function(res) {
		  	
			sendNotification('Logout to PC-'+term_id+'.','warning', '');

		  	axios.post('/student/terminal/logout',{term_id: term_id})
			.then(function(resp){
				window.location.reload();
			})
			.catch(function (err){
				showRespErrors(err.response.data);
			});
		});
	});

	//export PDF Lab ongoing lab users
	$('.btnLabExport').on('click', function(){

		window.open('/laboratory/export/'+$(this).prop('id')+'/users','_blank');
	});

	//Terminate All sessions
	$('.btnClearAll').on('click', function(){

		var lab_id = $(this).prop('id');
		swal({	
			title: 'Are you sure?', text: "You want to end all session?",
			type: 'warning', showCancelButton: true, confirmButtonColor: '#3085d6',
  			cancelButtonColor: '#d33', confirmButtonText: 'Yes, I confirm!'
		}).then(function(res) {
		  
		  	axios.post('/student/terminal/logout/all',{lab_id: lab_id})
			.then(function(resp){
				swal('Great!', 'All lab session has been terminated.','success');
			    showToastr('All lab session has been terminated.','success');
				window.location.reload();
			})
			.catch(function (err){
				showRespErrors(err.response.data);
			});
		});
	});
});

function setModalValues(title,name,faculty,subject,readonly){

	$('#mdlbl').text(title);
	$('[name="lab_name"]').val(name).prop('readonly',readonly);
	$('[name="lab_faculty"]').val(faculty);
	$('[name="lab_subject"]').val(subject);
	$('#mdlLaboratory').modal('show');

	if(readonly){
		$('#btnMdlLabSubmit').hide();
		$('#btnMdlLabUpdate').show();	
	} else {
		$('#btnMdlLabSubmit').show();
		$('#btnMdlLabUpdate').hide();
	}
}
