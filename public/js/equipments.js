$(document).ready(function(){

	initDataTable();
	
	//Add Equipment
	$('#btnAddEquipment').on('click', function(){

		setModalValues('Add New Equipment','','', '', '', '','MOUSE','OPERATIONAL','1',false);
	});

	$('#btnMdlEquipSubmit').on('click', function(){

		axios.post('/equipment/create',$('#frmMdlEquipment').serialize())
		.then(function(resp){
			swal('Great!', 'A new equipment has been added','success');
			sendNotification('Added a new equipment.','info', '');
			showToastr('A new equipment has been added.','success');
			$('#mdlEquipment').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});

	//Update Equipment
	$('.btnEquipUpdate').on('click', function(){

		axios.post('/equipment/info', {equip_id: $(this).prop('id')})
		.then(function(resp){

			var d = resp.data;
			setModalValues('Equipment ' + d.asset_code + ' Information',d.asset_code, 
				d.rfid_code, d.brand, d.model, d.serial_no,d.type,d.status,d.pc_id, true);
		});
	});

	$('#btnMdlEquipUpdate').on('click', function(){

		axios.post('/equipment/update',$('#frmMdlEquipment').serialize())
		.then(function(resp){
			swal('Great!', 'A new equipment has been updated','success');
			sendNotification('Updated an equipment.','info', '');
			showToastr('A new equipment has been updated.','success');
			$('#mdlEquipment').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});
});

//set init modal values
function setModalValues(title,asset,rfid,brand,model,serial,type,status,pc,readonly){

	$('#mdlbl').text(title);
	$('[name="asset_code"]').val(asset).prop('readonly',readonly);
	$('[name="rfid_code"]').val(rfid).prop('readonly',readonly);
	$('[name="brand"]').val(brand).prop('readonly',readonly);
	$('[name="model"]').val(model).prop('readonly',readonly);
	$('[name="serial_no"]').val(serial).prop('readonly',readonly);
	$('[name="type"]').val(type).change();
	$('[name="type"] option:not(:selected)').prop('disabled',readonly);
	$('[name="status"]').val(status).change();
	$('[name="connected_pc"]').val(pc).change();
	$('#mdlEquipment').modal('show');

	if(readonly){
		$('#btnMdlEquipSubmit').hide();
		$('#btnMdlEquipUpdate').show();	
	} else {
		$('#btnMdlEquipSubmit').show();
		$('#btnMdlEquipUpdate').hide();
	}
}