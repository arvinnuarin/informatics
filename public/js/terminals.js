$(document).ready(function(){

	//init Terminal Data table
	initDataTable();
	
	//Add Terminal
	$('#btnAddTerminal').on('click', function(){

		setModalValues('Add a New Terminal','', '', 1, false, 'nope');
	});

	$('#btnMdlTermSubmit').on('click', function(){

		axios.post('/terminal/create',$('#frmMdlTerminal').serialize())
		.then(function(resp){
			swal('Great!', 'A new terminal has been added','success');
			sendNotification('Added a new terminal.','info', '');
			showToastr('A new terminal has been added.','success');
			$('#mdlTerminal').modal('hide');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});

	//Update Terminal
	$('.btnTermUpdate').on('click', function(){
		axios.post('/terminal/info', {term_id: $(this).prop('id')})
		.then(function(resp){

			var d = resp.data;
			setModalValues('Terminal ' + d.pc_name + ' Information',d.pc_name, 
				d.ip_address, d.lab_id, true, 'granted');
		});
	});
});

//set init modal values
function setModalValues(title,pcname,ipadd,loc, readonly,update){

	$('#mdlbl').text(title);
	$('[name="pc_name"]').val(pcname);
	$('[name="pc_ipaddress"]').val(ipadd);
	$('[name="pc_location"]').val(loc).prop('selected', true);
	$('#mdlTerminal .form-control').prop('readonly', readonly)
	$('[name="shouldUpdate"]').val(update);
	$('#mdlTerminal').modal('show');
}