$(document).ready(function(){

	initDataTable(); //initialize data tables

	$('#btUpdatePass').on('click', function(){ // update password click event

		axios.post('/user/profile/update-pass',$('#frmUpdatePass').serialize())
		.then(function(resp){
			swal('Great!', 'Your password has been updated','success');
			showToastr('Password has been updated.','success');
			$('#frmUpdatePass')[0].reset();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
			$('#frmUpdatePass')[0].reset();
		});
	});

	//show add new user modal
	$('#btnAddUser').on('click', function(){
		$('#mdlUser').modal('show');
	});

	$('#btnMdlUserSubmit').on('click', function(){ //create new user

		axios.post('/user/create',$('#frmMdlUser').serialize())
		.then(function(resp){
			swal('Great!', 'You have created a new system user.','success');
			showToastr('A new user has been added.','success');
			window.location.reload();
		})
		.catch(function (err){
			showRespErrors(err.response.data);
		});
	});

	//unblock / block user
	$('.btnUserAction').on('click', function(){

		var status = $(this).attr('status');
		var type = '';
		if(status == 'emp'){
			type = 'block';
		} else {
			type = 'unblock';
		}

	swal({
		  title: 'Are you sure?',
		  text: "You want to "+type+" this user?",type: 'warning',
		  showCancelButton: true,confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',confirmButtonText: 'Yes, proceed!'
		}).then((result) => {
		  if (result.value) {
		    axios.post('/user/manage/role', {user_id: $(this).prop('id')})
			.then(function(resp){
				swal('Great!', 'A new user has been '+type+'ed.','success');
				showToastr('A new user has been '+type+'ed.','success');
				window.location.reload();
			})
			.catch(function (err){
				showRespErrors(err.response.data);
			});
		  }
		});
	});
});