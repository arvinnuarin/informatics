$(document).ready(function(){

	Echo.channel('informatics-notif')
    .listen('notifTrigger', (e) => {
        console.log(e);
    });

    axios.post('/notification/receive') //push notification to notif icon
	.then(function(resp){
		
		$('#notifi-list').empty();
		var cnt = 0;
		$.each(resp.data, function(k,v){

			$('#notifi-list').append(' <a href="" class="list-group-item">'+
                '<div class="media"><div class="media-body clearfix">'+
                '<div class="media-heading">'+v.message+'</div>'+
                '<p class="m-0"> <small>'+v.name+ ' @ '+v.created_at+'</small></p>'+
                '</div></div></a>');
			cnt++;
		});

		$('#notifi-count').text(cnt);
	});
});