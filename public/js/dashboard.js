$(document).ready(function(){

    //init Terminal Data table
	initDataTable();

	axios.post('/equipment/inventory/chart')
	.then(function(resp){
		var data = resp.data;
		
		new Chart($('#inventoryChart')[0].getContext('2d'), {
		    type: 'bar',
		    data: {
		        labels: ["Operational", "Unoperational", "Maintenance", "Lost/Stolen"],
		        datasets: [{
		            label: '# of Equipment',
		            data: [data.operational, data.unoperational, data.maintenance, data.lost],
		            backgroundColor: [
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)'
		            ],
		            borderColor: [
		                'rgba(54, 162, 235, 1)',
		                'rgba(255,99,132,1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        
		        animation: {
		        	duration: 3000
		            
		        },
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});

	});
});