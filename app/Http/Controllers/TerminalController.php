<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Terminal;
use App\Laboratory;

class TerminalController extends Controller
{
    
	public function __construct(){

        $this->middleware('auth');
    }

	public function index() {

		$terminals = Terminal::getTerminals();
		$locs = Laboratory::all();
		return view('layouts.terminal.index', compact('terminals', 'locs'));
	}

	public function create(Request $req){

		if($req->shouldUpdate == 'granted'){
			$req->validate(['pc_name'=>'required','pc_ipaddress'=>'ipv4','status'=>'required']);
		} else {
			$req->validate(['pc_name'=>'required','pc_ipaddress'=>'ipv4|unique:terminal,ip_address', 'status'=>'required']);
		}

		Terminal::updateOrCreate(['ip_address'=>$req->pc_ipaddress, 'pc_name'=>$req->pc_name], ['lab_id'=>$req->pc_location, 'status'=>$req->status]);
	}

	public function getTerminalInfo(Request $req){

		return response()->json(Terminal::find($req->term_id));
	}

}
