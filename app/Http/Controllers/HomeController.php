<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Terminal;
use App\Student;
use App\Equipment;
use App\Laboratory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware(['role:admin|emp|student']);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->hasRole(['admin', 'emp'])) {

            $pc_no = Terminal::count();
            $active_pc = Terminal::where('status','ONLINE')->count();
            $student_no = Student::count();
            $equip_no = Equipment::count();
            $equipments = Equipment::getEquipments();

            return view('home', compact('pc_no','active_pc','student_no','equip_no', 'equipments'));
        } else {

            $labs = Laboratory::getLaboratories();
            return view('layouts.laboratory.manage',compact('labs'));
        }
    }
}
