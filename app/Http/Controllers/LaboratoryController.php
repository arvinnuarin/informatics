<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laboratory;
use App\Student;
use App\Events\eventTrigger;

class LaboratoryController extends Controller
{
	public function __construct(){

        $this->middleware('auth');
    }

    public function manage(){
    
    	$labs = Laboratory::getLaboratories();
        return view('layouts.laboratory.manage',compact('labs'));
    }

    public function create(Request $req){

    	$req->validate(['lab_name'=>'required', 'lab_faculty'=>'required','lab_subject'=>'required']);

    	Laboratory::create(['name'=>$req->lab_name, 'faculty'=>$req->lab_faculty,'subject'=>$req->lab_subject]);
    }

    public function update(Request $req){

    	$req->validate(['lab_name'=>'required', 'lab_faculty'=>'required','lab_subject'=>'required']);

    	Laboratory::where('id',$req->lab_id)->update(['faculty'=>$req->lab_faculty, 'subject'=>$req->lab_subject]);
    }

    public function showInfo(Request $req){

    	return response()->json(Laboratory::find($req->lab_id));
    }

    public function show($id){ //show Laboratory status

        $labinfo = Laboratory::showLaboratory($id);
        $online = Laboratory::getOnlinePC($id)->online;
        $offline = Laboratory::getOfflinePC($id)->offline;
        try{
            $studpc = Student::getTerminalID(Student::getStudentID()->id)->terminal_id;
        } catch(\Exception $e){
            $studpc = 0;
        }
        
        return view('layouts.laboratory.showLab', compact('labinfo', 'studpc','online','offline'));
    }

    public function exportLabUsers($lab_id){

        $datas = Laboratory::showLaboratory($lab_id);
        return view('templates.labusers', compact('datas'));

      //return \PDF::loadView('templates.labusers', compact('datas'))->setPaper('a4')->setOrientation('portrait')->inline('LabUsers.pdf');
    }
}
