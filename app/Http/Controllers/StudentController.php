<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Attendance;
use App\User;
use App\Role;
use App\Terminal;

class StudentController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

	public function index() {

		$college = Student::where('level', 'COLLEGE')->get();
		$high = Student::where('level','HIGH SCHOOL')->get();

		return view('layouts.student.index', compact('college','high'));
	}

	public function create(Request $req){

		$validation = $req->validate(['stud_number'=>'unique:student_info,student_no','first_name'=>'required','last_name'=>'required', 'course'=>'required']);

		if($req->level == "COLLEGE"){
			Student::create(['student_no'=>$req->stud_number,'fname'=>$req->first_name,'lname'=>$req->last_name, 'mname'=>$req->middle_name, 'level'=>$req->level, 'course'=>$req->course]);
		} else {

			Student::create(['student_no'=>$req->stud_number,'fname'=>$req->first_name,'lname'=>$req->last_name, 'mname'=>$req->middle_name, 'level'=>$req->level, 'course'=>$req->course, 'grade'=>$req->grade, 'section'=>$req->section]);
		}

		$full_name = $req->first_name.' '.$req->last_name;
		$email = $req->stud_number.'@informatics.com';
		
		$student = User::create(['name'=>$full_name,'lname'=>$req->last_name,'fname'=>$req->first_name,'mname'=>$req->middle_name,'email'=>$email,'contact_no'=>$req->stud_number,'username'=>$req->stud_number, 'password'=>bcrypt('student2018')]);

		$stud_role = Role::where('name','student')->first();
       	$student->attachRole($stud_role); //attach student role 
	}

	public function getStudentInfo(Request $req){

		$info = Student::where('student_no', $req->stud_number)->first();
		return response()->json($info);	
	}

	public function setTerminal(Request $req){

		$stud_id = Student::getStudentID()->id;

		Attendance::create(['student_id'=> $stud_id, 'terminal_id'=>$req->term_id, 'attnd_status'=>'ONGOING']);
		Terminal::where('id',$req->term_id)->update(['status'=>'ONLINE']);
	}

	public function endTerminal(Request $req){

		//Attendance::where('terminal_id', $req->term_id)->update(['attnd_status'=>'COMPLETED']);
		Attendance::where('terminal_id', $req->term_id)->delete();
		Terminal::where('id',$req->term_id)->update(['status'=>'OFFLINE']);
		\Auth::logout(); //logout user*/
	}

	public function getTerminal($pc){ //show user terminal info
	
		$info = Student::getTerminalInfo($pc);
		try{
            $studpc = Student::getTerminalID(Student::getStudentID()->id)->terminal_id;
        } catch(\Exception $e){
            $studpc = 0;
        }

		if($info){
			return view('layouts.laboratory.showUser', compact('info','studpc'));
		} else{
			return redirect()->route('home');
		}
	}

	public function endAllTerminal(Request $req){ //end all sessions

		$pcs = Terminal::join('attendance AS attnd', 'attnd.terminal_id', 'terminal.id')->select('terminal_id')->where('terminal.lab_id', $req->lab_id)->where('attnd.attnd_status','ONGOING')->get();

		foreach ($pcs as $pc) {
			
			//Attendance::where('terminal_id', $pc->terminal_id)->update(['attnd_status'=>'COMPLETED']);
			Attendance::where('terminal_id', $req->term_id)->delete();
			Terminal::where('id',$pc->terminal_id)->update(['status'=>'OFFLINE']);
		}
	}
}
