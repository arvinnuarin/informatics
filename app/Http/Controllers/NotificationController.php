<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\notifTrigger;
use App\Notification;

class NotificationController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function showCenter(){

    	$notifs = Notification::join('users', 'users.id','notifications.user_id')->select('notifications.*','users.name')->take(10)->orderBy('updated_at', 'DESC')->get();

    	return view('layouts.notification.center', compact('notifs'));
    }

    public function send(Request $req){ //send notification

    	$user = \Auth::user();
    	$message = Notification::create(['user_id'=>$user->id, 'type'=>$req->type, 'message'=>$req->message,'url_link'=>$req->url_link]);
		
		event(new notifTrigger($message, $user));
    }

    public function receive(){ //get 10 notification

    	$notifs = Notification::join('users', 'users.id','notifications.user_id')->select('notifications.*','users.name')->take(5)->orderBy('updated_at', 'DESC')->get();

    	return response()->json($notifs);
    }
}
