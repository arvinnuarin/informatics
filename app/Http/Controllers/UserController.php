<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function showProfile(){

    	return view('layouts.settings.profile');
    }

    public function setNewPassword(Request $req){ //update user password

    	$messages = ['cur_pass.required'=> 'Please input current password.', 'new_pass.required'=> 'Please input new password.','new_pass_confirmation.required'=> 'Please confirm new password.', 'cur_pass.current_password'=> "The password you entered doesn't match."];

		$validation = $req->validate(['cur_pass'=>'required|current_password','new_pass'=>'required|string|min:8|confirmed','new_pass_confirmation'=>'required'],$messages);

		$user = \Auth::user();
		$user->password = bcrypt($req->new_pass);
		$user->save();
    }

    public function manageUsers(){

        $users = User::whereHas('roles', function($q){$q->where('name', 'emp')->orWhere('name', 'ban');})->get();
        return view('layouts.settings.manageUsers', compact('users'));
    }

    public function create(Request $req){ //create a new employee User

        $messages = ['lname.required'=>'Please input last name.','fname.required'=>'Please input first name.','email.required'=>'Email Address is required.', 'contact.required'=>'Contact No. is required.','username.required'=>'Username is required'];

      $req->validate(['lname'=>'required','fname'=>'required','email'=>'required|email|unique:users,email','contact'=>'required|digits:11|unique:users,contact_no', 'username'=>'required|unique:users,username|min:5'], $messages);

       $full_name = $req->fname. ' '.$req->lname;
       
       $emp = User::create(['name'=>$full_name,'lname'=>$req->lname,'fname'=>$req->fname,'mname'=>$req->mname,'email'=>$req->email,'contact_no'=>$req->contact,'username'=>$req->username, 'password'=>bcrypt('secret2017')]);

       $emp_role = Role::where('name','emp')->first();
       $emp->attachRole($emp_role); //attach employee role 
    }

    public function setRole(Request $req){

      $emp = User::find($req->user_id);
      $cur_role = $emp->roles[0]->name;
      $new_role = 'emp';

      if($cur_role == 'emp'){$new_role = 'ban';} 
      else if ($cur_role == 'ban'){$new_role = 'emp';}

      $emp_role = Role::where('name', $new_role)->first();
      $emp->detachRoles($emp->roles);
      $emp->attachRole($emp_role);
    }
}
