<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Terminal;
use App\Equipment;

class EquipmentController extends Controller
{
    public function __construct(){

        //$this->middleware(['role:admin|emp']);
        $this->middleware('auth');
    }

    public function showInventory(){

        $inventory = Equipment::getInventory();
        $pcs = Terminal::all();
        return view('layouts.equipment.inventory',compact('pcs', 'inventory'));
    }

    public function showInventoryGraph(){

        return response()->json(Equipment::getInventoryChartData());
    }

    public function showStatus(){
    	
        $pcs = Terminal::all();
        $equipments = Equipment::getEquipments();
        return view('layouts.equipment.status', compact('pcs','equipments'));
    }

    public function showEquipmentData(Request $req){

        return response()->json(Equipment::getEquipment($req->equip_id));
    }

    public function create(Request $req){

        $req->validate(['asset_code'=>'required|unique:equipment,asset_code', 'rfid_code'=>'required|unique:equipment,rfid_code','brand'=>'required','model'=>'required','serial_no'=>'required|unique:equipment,serial_no','type'=>'required','status'=>'required','connected_pc'=>'required']);

        Equipment::create(['asset_code'=>$req->asset_code,'rfid_code'=>$req->rfid_code,'brand'=>$req->brand,'model'=>$req->model,'serial_no'=>$req->serial_no,'type'=>$req->type,'status'=>$req->status,'terminal_id'=>$req->connected_pc]);
    }

    public function update(Request $req){

        $req->validate(['status'=>'required','connected_pc'=>'required','asset_code'=>'required']);

        Equipment::where('asset_code',$req->asset_code)->update(['status'=>$req->status,'terminal_id'=>$req->connected_pc]);
    }
}
