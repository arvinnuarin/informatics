<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentTerminal extends Model
{
    protected $table = 'student_terminal';
    protected $fillable = ['terminal_id','student_id'];
}
