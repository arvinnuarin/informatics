<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;
    protected $table = 'student_info';
    protected $fillable = ['student_no','fname','lname','mname','level','course', 'grade', 'section'];

    public static function getStudentID(){ //get auth student id

    	return static::select('id')->where('student_no',\Auth::user()->username)->first();
    }

    public static function getTerminalID($studID){
			
		return static::join('attendance AS attnd','student_info.id','attnd.student_id')->select('attnd.terminal_id')->where('student_info.id', $studID)->where('attnd.attnd_status', 'ONGOING')->first();
    }

    public static function getTerminalInfo($pc){

        return static::join('attendance AS attnd', 'student_info.id', 'attnd.student_id')->join('terminal AS term','term.id','attnd.terminal_id')->select('student_info.*','term.pc_name','term.id AS pcid','term.ip_address','term.status','term.id AS term_id','attnd.created_at')->where('attnd.attnd_status', 'ONGOING')->where('attnd.terminal_id',$pc)->first();
    }

}
