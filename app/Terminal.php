<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    public $timestamps = false;
    protected $table = 'terminal';
    protected $fillable = ['pc_name','ip_address','lab_id','status'];

    public static function getTerminals(){

    	return static::join('laboratory AS lab', 'lab.id', 'terminal.lab_id')->select('terminal.*', 'lab.name AS location')->get();
    } 
}
