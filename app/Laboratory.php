<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laboratory extends Model
{
    public $timestamps = false;
    protected $table = 'laboratory';
    protected $fillable = ['name','faculty','subject'];


    public static function getLaboratories(){

    	return static::leftJoin('terminal AS term', 'laboratory.id','term.lab_id')->selectRaw('laboratory.*, COUNT(term.id) AS pc_nos')->groupBy('laboratory.id')->get();
    }

    public static function getOnlinePC($id){

    	return static::leftJoin('terminal AS term', 'laboratory.id','term.lab_id')->selectRaw('COUNT(term.id) AS online')->where('laboratory.id',$id)->where('term.status','ONLINE')->first();
    }

    public static function getOfflinePC($id){

    	return static::leftJoin('terminal AS term', 'laboratory.id','term.lab_id')->selectRaw('COUNT(term.id) AS offline')->where('laboratory.id',$id)->where('term.status','!=','ONLINE')->first();
    }

    public static function showLaboratory($id){

    	return static::join('terminal AS term', 'laboratory.id', 'term.lab_id')->leftJoin('attendance AS attnd', 'attnd.terminal_id','term.id')->leftJoin('student_info AS stud','stud.id','attnd.student_id')->select('laboratory.faculty','laboratory.subject','laboratory.name', 'laboratory.id AS lab_id', 'term.*', 'stud.fname','stud.lname','stud.student_no', 'attnd.created_at')->where('laboratory.id', $id)->orderBy('term.id')->get();
    }
}
