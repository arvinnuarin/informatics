<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    public $timestamps = false;
    protected $table = 'equipment';
    protected $fillable = ['terminal_id', 'asset_code','rfid_code','brand','model','type','serial_no','status'];

    public static function getEquipments(){

    	return static::join('terminal AS pc', 'equipment.terminal_id','pc.id')->select('equipment.*','pc.pc_name', 'pc.id AS pc_id')->get();
    }

    public static function getEquipment($equip_id){

    	return static::join('terminal AS pc', 'equipment.terminal_id','pc.id')->select('equipment.*','pc.pc_name','pc.id AS pc_id')->where('equipment.id',$equip_id)->first();
    }

    public static function getInventory(){

    	return static::selectRaw('type, brand, model, COUNT(status) AS total, sum(case when status = "OPERATIONAL" then 1 else 0 end) operational, sum(case when status = "UNOPERATIONAL" then 1 else 0 end) unoperational, sum(case when status = "MAINTENANCE" then 1 else 0 end) maintenance, sum(case when status = "LOST" then 1 else 0 end) lost')->groupBy('model')->get();
    }

    public static function getInventoryChartData(){

        return static::selectRaw('COUNT(status) AS total, sum(case when status = "OPERATIONAL" then 1 else 0 end) operational, sum(case when status = "UNOPERATIONAL" then 1 else 0 end) unoperational, sum(case when status = "MAINTENANCE" then 1 else 0 end) maintenance, sum(case when status = "LOST" then 1 else 0 end) lost')->first();
    }

}
